from pathlib import Path

import dotenv
from pydantic import BaseSettings

BASE_DIR = Path(__file__).resolve().parent.parent


class Settings(BaseSettings):
    DEBUG: bool = False
    DEV: bool = True

    TG_API_TOKEN: str

    class Config:
        env_file = Path(BASE_DIR, 'settings', 'env')
        dotenv.load_dotenv(env_file)


settings = Settings()
