import logging

from aiogram.utils import executor

from bot.app import routers
from bot.bot import register_routers

logging.basicConfig(level=logging.INFO)


if __name__ == '__main__':
    executor.start_polling(register_routers(routers=routers), skip_updates=False)
