from dataclasses import dataclass
from typing import List

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from bot.middlewares import AccessMiddleware
from settings.config import settings


@dataclass
class CommandRouter:
    dp: Dispatcher


def register_routers(routers: List[CommandRouter]) -> Dispatcher:
    return routers[0].dp


bot = Bot(token=settings.TG_API_TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())
dp.middleware.setup(AccessMiddleware())
